#!/bin/bash

set -xe

# Connect to the staging VM through SSH and execute the following steps:
# - login into the GitLab Container Registry
# - remove a previous running container (if exists)
# - pull the latest image
# - run the app container
# - exit
ssh -o PreferredAuthentications=publickey -p $STAGING_SSH_PORT $STAGING_USER@$STAGING_IP \
"docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY \
&& docker rm --force my-dummy-pipeline || true \
&& docker pull $CONTAINER_IMAGE:$CI_COMMIT_REF_NAME \
&& docker run -d --restart on-failure:5 --name my-dummy-pipeline --network mongodb_net -p 80:8080 $CONTAINER_IMAGE:$CI_COMMIT_REF_NAME \
&& exit"
