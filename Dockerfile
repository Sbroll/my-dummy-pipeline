FROM openjdk:8-jdk-slim
ENV TZ=Europe/Rome

RUN apt-get update && apt-get install -y curl

WORKDIR /srv/app/
COPY ./target/my-dummy-pipeline.jar ./app.jar

EXPOSE 8080
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom", "-jar", "app.jar"]
HEALTHCHECK --interval=1m --timeout=3s CMD curl -f http://localhost:8080/_meta/health || exit 1
