package it.unimib.disco.lta.mydummypipeline;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyDummyPipelineApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyDummyPipelineApplication.class, args);
	}

}
