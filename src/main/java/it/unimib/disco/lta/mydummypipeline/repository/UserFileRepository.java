package it.unimib.disco.lta.mydummypipeline.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import it.unimib.disco.lta.mydummypipeline.domain.UserFile;

@RepositoryRestResource(collectionResourceRel = "user_files", path = "user-files")
public interface UserFileRepository extends MongoRepository<UserFile, String> {
  
}
